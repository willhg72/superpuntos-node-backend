"use strict";

var _app = _interopRequireDefault(require("./app"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import './database';
_app["default"].listen(3000, function () {
  console.log('Server listening at port 3000 ');
});