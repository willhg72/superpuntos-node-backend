import express from 'express';
import morgan from 'morgan';

//creo el objeto general de la app
const app = express();

//uso morgan para conocer las peticiones
app.use(morgan('dev'));

app.get('/', (req, res)=> {
    res.json('Hola Mundo')
})

export default app